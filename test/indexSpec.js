/* eslint-disable no-unused-expressions */

const assert = require('assert');
const expect = require('chai').expect;
const should = require('chai').should;
const request = require('../node_modules/supertest');
const app = require('../index');
const req = request(app);
const fs = require('fs');

describe('coding challenge spec', () => {

  it('should return json for english and spanish', function ()  {    
    langModule.setFileName('./test/testLanguages.json');
    return request(app)
      .get('/languageList')
      .then(function(response){
        assert.equal(response.status, 200);
        expect(response.text).to.contain('[{"English":"English","alpha2":"en"},{"English":"Spanish; Castilian","alpha2":"es"},{"English":"Elf","alpha2":"el"}]');
    })
  });

  it('should return true for english', function ()  {    
    langModule.setFileName('./test/testLanguages.json');
    return request(app)
      .get('/validateLanguage/en')
      .then(function(response){
        assert.equal(response.status, 200);
        expect(response.text).to.contain('true');
      })
  });

  it('should return true for Spanish', function()  {    
    langModule.setFileName('./test/testLanguages.json');
    return request(app)
      .get('/validateLanguage/es')
      .then(function(response){
        assert.equal(response.status, 200);
        expect(response.text).to.contain('true');
      })
  })

  it('should return false for Klingon', function()  {    
    langModule.setFileName('./test/testLanguages.json');
    return request(app)
      .get('/validateLanguage/klingon')
      .then(function(response){
        assert.equal(response.status, 200);
        expect(response.text).to.contain('false');
      })
  })

  it('should return all glossary items', function()  {    
    langModule.setFileName('./test/testLanguages.json');
    glossaryModule.setFileName('./test/testGlossary.json');
    return request(app)
      .get('/glossary')
      .then(function(response){
        assert.equal(response.status, 200);
        expect(response.text).to.contain('{"glossary":[{"path":"","url":"","content":"","relative_path":"","id":"","collection":"","excerpt":"","draft":false,"categories":[],"experience":"","type":"","published":true,"title":"QSEHRA","meta-title":"QSEHRA","lang":"es","layout":"glossary-word","tags":["glossary"],"page_name":"QSEHRA","page_type":"glossary","page_topic":"help","page_lifecycle":"all","page_audience":"all","page_category":"glossary","date":"","slug":"qsehra","ext":".md"},{"path":"","url":"","content":"","relative_path":"","id":"","collection":"","excerpt":"","draft":false,"categories":[],"experience":"","type":"","published":true,"title":"QSEHRA","meta-title":"QSEHRA","lang":"en","layout":"glossary-word","tags":["glossary"],"page_name":"QSEHRA","page_type":"glossary","page_topic":"help","page_lifecycle":"all","page_audience":"all","page_category":"glossary","date":"","slug":"qsehra","ext":".md"}]}');
      })
  })

  it('should return all spanish glossary items', function()  {    
    langModule.setFileName('./test/testLanguages.json');
    glossaryModule.setFileName('./test/testGlossary.json');
    return request(app)
      .get('/glossary/es')
      .then(function(response){
        assert.equal(response.status, 200);
        expect(response.text).to.contain('[{"path":"","url":"","content":"","relative_path":"","id":"","collection":"","excerpt":"","draft":false,"categories":[],"experience":"","type":"","published":true,"title":"QSEHRA","meta-title":"QSEHRA","lang":"es","layout":"glossary-word","tags":["glossary"],"page_name":"QSEHRA","page_type":"glossary","page_topic":"help","page_lifecycle":"all","page_audience":"all","page_category":"glossary","date":"","slug":"qsehra","ext":".md"}]' );
    })
  })

  it('should return all english glossary items', function()  {    
    langModule.setFileName('./test/testLanguages.json');
    glossaryModule.setFileName('./test/testGlossary.json');
    return request(app)
      .get('/glossary/en')
      .then(function(response){
        assert.equal(response.status, 200);
        expect(response.text).to.contain('[{"path":"","url":"","content":"","relative_path":"","id":"","collection":"","excerpt":"","draft":false,"categories":[],"experience":"","type":"","published":true,"title":"QSEHRA","meta-title":"QSEHRA","lang":"en","layout":"glossary-word","tags":["glossary"],"page_name":"QSEHRA","page_type":"glossary","page_topic":"help","page_lifecycle":"all","page_audience":"all","page_category":"glossary","date":"","slug":"qsehra","ext":".md"}]');
    })
  })

  it('should  message if language is not supported', function()  {    
    langModule.setFileName('./test/testLanguages.json');
    glossaryModule.setFileName('./test/testGlossary.json');
    return request(app)
      .get('/glossary/klingon')
      .then(function(response){
        assert.equal(response.status, 200);
        expect(response.text).to.contain('The selected language is not valid.');
    })
  })

  it('should  message if language is supported but no files', function()  {    
    langModule.setFileName('./test/testLanguages.json');
    glossaryModule.setFileName('./test/testGlossary.json');
    return request(app)
      .get('/glossary/el')
      .then(function(response){
        assert.equal(response.status, 200);
        expect(response.text).to.contain('Glossary is not available in this language.');
    })
  })

  it('language moudle file name should change', function()  {    
    langModule.setFileName('./test/testLanguages.json');
    expect(langModule.fileName()).to.contain('./test/testLanguages.json');
  })

  it('glossary moudle file name should change', function()  {    
    glossaryModule.setFileName('./test/testGlossary.json');
    expect(glossaryModule.fileName()).to.contain('./test/testGlossary.json');
  })

  it('glossary moudle file name should change', function()  {    
    glossaryModule.setURL('./test/testGlossary.json');
    expect(glossaryModule.getURL()).to.contain('./test/testGlossary.json');
  })

  it('Should load the glossary file', function()  {  
    glossaryModule.setURL('https://www.healthcare.gov/api/glossary.json');
    glossaryModule.setFileName('C:/back-end/test/testGlossary.json');
    glossaryModule.createFile(true);
    expect(JSON.stringify(glossaryModule.getGlossary())).to.contain('{"glossary":[{"path":"","url":"","content":"","relative_path":"","id":"","collection":"","excerpt":"","draft":false,"categories":[],"experience":"","type":"","published":true,"title":"QSEHRA","meta-title":"QSEHRA","lang":"es","layout":"glossary-word","tags":["glossary"],"page_name":"QSEHRA","page_type":"glossary","page_topic":"help","page_lifecycle":"all","page_audience":"all","page_category":"glossary","date":"","slug":"qsehra","ext":".md"},{"path":"","url":"","content":"","relative_path":"","id":"","collection":"","excerpt":"","draft":false,"categories":[],"experience":"","type":"","published":true,"title":"QSEHRA","meta-title":"QSEHRA","lang":"en","layout":"glossary-word","tags":["glossary"],"page_name":"QSEHRA","page_type":"glossary","page_topic":"help","page_lifecycle":"all","page_audience":"all","page_category":"glossary","date":"","slug":"qsehra","ext":".md"}]}');
  })

  it('Should load the language files', function()  {  
    langModule.setFileName('C:/back-end/test/testLanguages.json');
    langModule.createFile(true);
    expect(JSON.stringify(langModule.getLanguageList())).to.contain('[{"English":"English","alpha2":"en"},{"English":"Spanish; Castilian","alpha2":"es"},{"English":"Elf","alpha2":"el"}]');
  })

  it('Should return the language files', function()  {  
    langModule.setFileName('C:/back-end/test/testLanguages.json');
    langModule.getLanguageList();
    expect(JSON.stringify(langModule.getLanguageList())).to.contain('[{"English":"English","alpha2":"en"},{"English":"Spanish; Castilian","alpha2":"es"},{"English":"Elf","alpha2":"el"}]');
  })

});
