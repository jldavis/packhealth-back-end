//Export module for the glossary
const fs = require('fs');
const request = require('request');
const jsonQuery = require('../node_modules/json-query');
const language = require('../export_modules/languages');
var glossaryFile = './glossary.json';
const writer = fs.createWriteStream(glossaryFile);
const getJSON = require('../node_modules/get-json');
var nock = require('nock');
var url = 'https://www.healthcare.gov/api/glossary.json';

var glossary = module.exports = {}

glossary.fileName = function(){
    return glossaryFile;
 };
 
glossary.setFileName = function(name){
    glossaryFile = name;
 };

 glossary.getURL = function(){
    return url;
 };

 glossary.setURL = function(name){
    url = name;
 };


glossary.createFile = function(isMock){

    var jsonContent = fs.readFileSync(glossaryFile);
    if(isMock){
        result = nock(url, {
            reqheaders: {
                "content-type": "application/json",
            }
        })
        .get(url)
        .reply(200, jsonContent);
        writer.write(jsonContent);
    }
    else{
    request(glossary.getURL(), function (error, response, body) {
        writer.write(JSON.stringify(JSON.parse(body)));
     })
    }
 }

glossary.getGlossary = function(lang){
    if(!lang){
        return JSON.parse(fs.readFileSync(glossaryFile));
    }
    
    if(language.validateLanguage(lang)){
        var file = JSON.parse(fs.readFileSync(glossaryFile));
        var result = jsonQuery('glossary[*lang=' + lang + ']', {data: file});
        if(result.value.length > 0){
            return result.value;
        }
        else{
            return 'Glossary is not available in this language.';
        }
    }
    else{
        return 'The selected language is not valid.';
    }
}

 