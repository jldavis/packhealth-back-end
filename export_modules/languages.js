//Export module for languages

const fs = require('fs');
const request = require('request');
var langFile = './languages.json';
const writer = fs.createWriteStream(langFile);
const getJSON = require('../node_modules/get-json');
var nock = require('nock');

var language = module.exports = {}

//url to language codes
const url = "https://pkgstore.datahub.io/core/language-codes/language-codes_json/data/734c5eea7e10548144a18241e8f931f8/language-codes_json.json"

language.fileName = function(){
   return langFile;
};

language.setFileName = function(name){
   langFile = name;
};

//Return a list of all languages supported by ISO-639-1
language.getLanguageList = function (){
   return JSON.parse(fs.readFileSync(langFile));
}

//Validate that the language is supported by ISO-639-1.  Only accepting alpha2 so return out if length > 2.
language.validateLanguage = function(language){
   var file = JSON.parse(fs.readFileSync(langFile));
   var resp = false;
   var returnedData = $.grep(file, function (element, index) {
      var item;
      if(language.length == 2){
         item = element.alpha2 == language;
      }
      if(item ){
         resp = true;
      }
  });
      return resp;
}

//Write JSON file to list so we don't have to hold it in memory.  If it does exist, overwrite it.
language.createFile = function(isMock){
      var jsonContent = fs.readFileSync(langFile);
      if(isMock){
          jsonContent = fs.readFileSync(langFile);
          result = nock(url, {
              reqheaders: {
                  "content-type": "application/json",
              }
          })
          .get(url)
          .reply(200, jsonContent);
      }
      else{
      request(url, function (error, response, body) {
         writer.write(JSON.stringify(JSON.parse(body)));
       })
      }
   }
