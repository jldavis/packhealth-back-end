const request = require('request-promise-native');

const url = 'https://www.healthcare.gov/api/glossary.json';
const app = require('./index_old');
global.langModule= require('./export_modules/languages');
global.glossaryModule = require('./export_modules/glossary');
const jsdom = require("jsdom");
const dom = new jsdom.JSDOM(`<!DOCTYPE html>`);
global.$ = require('./node_modules/jquery')(dom.window);

// return an array of healthcare.gov glossary items for the language specified
// if no language is specified, return an array of all glossary items.
exports.handler = async (language) => {
    glossaryModule.createFile(true);
    console.log(glossaryModule.getGlossary(language));
};