const express = require('./node_modules/express');
const fs = require('fs');
const app = express();
global.langModule= require('./export_modules/languages');
global.glossaryModule = require('./export_modules/glossary');
const jsdom = require("jsdom");
const dom = new jsdom.JSDOM(`<!DOCTYPE html>`);
global.$ = require('./node_modules/jquery')(dom.window);

module.exports = app;

//Get a list of supported languages
app.get('/languageList', (req, res) => {
  fs.readFile(langModule.fileName(), "utf8", function(err, data){
    res.status(200).json(JSON.parse(data));
  }); 
});

//See if a alpha2 language is supported
app.get('/validateLanguage/:lang', (req, res) => {
  res.status(200).send(JSON.parse(langModule.validateLanguage(req.params.lang)));
});

//Get Glossary using alpha2 language
app.get('/glossary/:lang?', (req, res) => {
  res.status(200).send(glossaryModule.getGlossary(req.params.lang));
});

app.listen(8000, () => {
  //Creating files when server starts.
  langModule.createFile(false);
  glossaryModule.createFile(false);
  console.log("Server started. Files Created.");
 })
 
